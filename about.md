---
layout: page
title: About
permalink: /about/
---

Symbiosity will offer commercial/industrial real estate owners/operators a fire detection monitoring and safety solution through our Emergency Control Centre to monitor and manage their fire risk and fire detection systems and then liaison real time fire detection and emergencies to the nearest local fire department to respond to the location. 

Symbiosity will install, maintain and monitor the fire detection management system on behalf of the customers, while giving them innovative technology back with specialist who have the expertise to monitor and analyze the potential threats. 

The global fire and safety market is worth $66 billion dollars. A recent report published by Markets and Markets, a global market research and consulting company based in the USA, has forecast that the fire protection systems market will grow from US$34.74 billion in 2013 to US$66.56 billion in 2018, representing a compound annual growth rate (CAGR) of 13.9 percent during the forecast period. 

Symbiosity will offer real time fire detection data via a web based application, while reducing cost. The systems will produce an early “pre-alarm” warning, based on the level of detection, which allows the responsible person to investigate potential alarms before the system activates its fire alarm. The system will provide information of the number of occupants inside the building, so that rescue teams can effect rescue activities. 

The web based application will facilitate remote monitoring by fire and safety staff, generate real time reports, map location with clearly defined floor plan, displaying routes to the distress location, as well as nearby standpipe/hydrants, building wet/dry risers, stairway and emergency shutoff locations and systems throughout the building. 

Symbiosity will offer addon services such as consulting, building evacuation planning, fire system layout, suppression system, sprinkler and fire extinguisher layout. Customers pay a tiered subscription fee quarterly based on the building square footage and get their building equipped and setup with the *hardwares and *software at no additional initial cost, saving them the capital to purchase the hardware devices and software. 

Symbiosity Fire Detection Management System is currently at the conceptual stage, researching the hardware and software solutions.
